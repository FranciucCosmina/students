using students.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace students.Repository.Interfaces
{
  public interface IStudentRepository
  {
    IEnumerable<Student> GetStudents();
    Student GetStudent(int id);
    void AddStudent(Student student);

  }
}
