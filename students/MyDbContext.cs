using Microsoft.EntityFrameworkCore;
using students.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace students
{
  public class MyDbContext:DbContext
  {
    public MyDbContext(DbContextOptions options):base(options)
    { }
    public DbSet<Student> Students;
    public DbSet<CourseDetail> CourseDetails;
    public DbSet<Course> Courses;
    protected override void OnModelCreating(ModelBuilder model)
    {
      model.Entity<CourseDetail>().HasKey(c => new { c.CourseId, c.StudentId });
    }
  }
}
