using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace students.Models
{
  public class Course
  {
    public Course()
    {
      Students = new List<CourseDetail>();
    }
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }
    public string Name { get; set; }
    public string Professor { get; set; }
    public List<CourseDetail> Students { get; set; }
    
  }
}
