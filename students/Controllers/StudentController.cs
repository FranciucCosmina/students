using Microsoft.AspNetCore.Mvc;
using students.Models;
using students.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace students.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class StudentController : Controller
  {
    public IStudentRepository _studentRepository;
    public StudentController(IStudentRepository studentRepository)
    {
      _studentRepository = studentRepository ?? throw new ArgumentNullException(nameof(studentRepository));
    }
    [HttpGet("{int id}")]
    public Student GetStudent(int id)
    {
      return _studentRepository.GetStudent(id);
    }
    [HttpGet]
    public IEnumerable<Student>GetStudents()
    {
      return _studentRepository.GetStudents();
    }
    [HttpPost]
    public void AddStudent([FromBody]Student student)
    {
      _studentRepository.AddStudent(student);
    }
  }

}
